# SSH

```shell
ssh-keygen -t ed25519 -C "your_email@example.com"
# Add pub key to github account
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_ed25519
git ...
```
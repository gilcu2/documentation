# Git commands

* Change default upstreams repo

```sh
    git branch -u <repo>
    git branch -vv
```

* Submodules

    ```sh
    git clone --recursive <URL> # Project with submodules
    git submodule update --init # download submodules and initalize
    git pull --recurse-submodules # pull including submodules
    git submodule update --remote # pull only submodules
    git submodule add -b master [--name name] <URL> [path]; git submodule init # Add submodule and initialize
    git submodule sync 
  # Delete
    git submodule deinit -f <mymodule>
    rm -rf .git/modules/<mymodule>
    git rm -f <mymodule>
    ```
  

* Delete remote
  git push -d <remote_name> <branchname>


# Comparative of async backend http libraries

Async backend libraries allows to server http request without blocking cpu threads,
so they allow to serve thousands of request per second.
The comparative will be done with the follows parameters:

- Easiness:
  - Setup project
  - Refactoring
  - Building
  - Documentation
  - IDE
- Understandability
  - Burocracy
  - Routing
  - Type checking
  - Concurrency
- Maturity
- Security
- Throughput
  - Parallelism
  - Native
- Features
  - Json support
  - Parameter validation
  - Authentication

For the comparison the classical url shortener problem using redis as backend

## Selected libraries

- Quart/Python: https://github.com/gilcu2/url_shortener_quart_python
- Ktor/Kotlin
- Akka/Scala: https://github.com/gilcu2/url_shortener_akka_scala3
- Beego/Go
- Rocket/Rust

## Summary table

| Lib                   | Quart/Python | Ktor/Kotlin | Akka/Scala | Beego/Go | Rocket/Rust |
|-----------------------|:------------:|:-----------:|:----------:|:--------:|:-----------:|
| **Easiness**          |      5       |             |            |          |             |
| Documentation         |      5       |      4      |     3      |    2     |      5      |
| Setup project         |      5       |      4      |     4      |    3     |      5      |
| Refactoring           |      3       |      5      |            |          |             |
| Building              |      5       |      4      |     3      |    5     |      4      |
| IDE                   |      5       |      5      |     5      |    5     |      4      |
| **Understandability** |      4       |             |            |          |             |
| Burocracy             |      5       |      4      |     2      |    4     |      4      |
| Routing               |      5       |      3      |     3      |    4     |      4      |
| Type checking         |      3       |      5      |     5      |    5     |      5      |
| **Maturity**          |      3       |      4      |     5      |    4     |      3      |
| **Security**          |      2       |             |            |          |      5      |
| **Throughput**        |      3       |             |            |          |             |
| Parallelism           |      2       |      4      |     3      |    4     |      5      |
| Native                |      1       |      4      |     4      |    5     |      5      |
| **Features**          |      5       |             |            |          |             |

## Quart/Python

- Documentation: So easy to follow and create first project
- Setup project: Only import modules,Server in 3 lines
- Refactoring: Fails (Pycharm). Needs checks with linters, type_checkers, test
- Building: Fast
- Burocracy: Only exactly what you need
- Routing: Directly associated with functionality in top level
- Maturity: Examples don't work after add schema validation module quart-schema
- Parameter validation

## Ktor/Kotlin

- Documentation: Need to research/discover how to achieve without Intellij Ultimate
- Setup project : Easy only with Intellij Ultimate.
  With community crate project in start.ktor.io and import with gradle.
  Hard to done manually
- Refactoring: ok
- Building: Slow first time
- Burocracy: Associated in second level call
- Routing: Associated in second level call
- Maturity:
- Parameter validation:

## Akka/Scala

- Documentation: Hard to follow
- Setup project : Can be done manually,
- Refactoring: ok
- Building: Slower thatn other
- Burocracy: Associated in second level call
- Routing: Associated in second level call
- Maturity:
- Parameter validation

## Beego/Go

- Documentation: Mistakes, not clear
- Setup project : Easy given good documentation
- Refactoring: ok
- Building: Fast
- Burocracy: Associated in second level call
- Routing: Associated in first level call. Looks simpler than Kotlin/Scala
- Maturity:
- Parameter validation:

## Rocket/Rust

- Documentation: Very good. So easy to follow and create first project
- Setup project : Easy
- Refactoring: ok
- Building: Very fast, slow first time
- IDE: Full support payed Clion, Intellij no debugging
- Burocracy: Associated in second level call
- Routing: Associated in first level call
- Maturity:
- Parameter validation: ok
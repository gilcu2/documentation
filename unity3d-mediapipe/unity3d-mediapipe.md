# Unity3d-mediapipe plugin

## Install

```shell
git clone git@github.com:homuler/MediaPipeUnityPlugin.git
cd MediaPipeUnityPlugin
python build.py build --desktop gpu --opencv cmake -vv
# Open project in unity3d hub
# Open Assets/Mediapipe/Samples/Scenes/Start Scene
# Configure BootStrap and Image Source for your source. 
# In case you want change the default camera use camera in Assets/Mediapipe/Samples/Common/Scripts/ImageSource/WebCamSource.cs 
# Start method, webCamDevice = availableSources[4]; you can try different cameras
```

## Use from another project

Install in some place (can be as a submodule) and add your project as a local package
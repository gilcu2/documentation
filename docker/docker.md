# Docker

* Build img: docker build -t \<name> \<dir with Dockerfile>
* Run server set in dockerfile: docker run -d -p 5000:5000 \<img _name[:\<tag>]>
* Run interactive shell: docker run -i -t <img_name> /bin/bash
* Shell in a running container: docker exec -it <container name or id> /bin/bash
* Stop all containers: docker kill $(docker ps -q)
* Remove all containers: docker rm $(docker ps -a -q)
* Clean images: docker image prune -a
* Clean everything:  docker system prune -a

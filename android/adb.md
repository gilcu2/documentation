# Adb command

- Allows connect to Android device for development over wifi
  1. Adb connection over usb already setup
  2. Connect device through Usb, "adb devices"must show it
  3. adb tcpip 5555
  4. Disconnect USB
  5. Find your phone's IP address in Settings > About Phone > Status > IP Address
  6. abd connect \<Device IP>
  7. "adb devices" must show the device IP

- Change screen off timeout

  adb shell settings put system screen_off_timeout <TIMEOUT_MS>
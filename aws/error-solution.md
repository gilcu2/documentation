# Error solution in aws

*  The security token included in the request is expired:

    In console/\<your user menu>/[My Security credential](https://console.aws.amazon.com/iam/home?nc2=h_m_sc#/security_credentials) create new access keys and update .aws/credentials

* Multiprofile: Can be selected using:
    ```sh
    export AWS_PROFILE=xxx
    ```

# JetBrain IDEs

- No merged buttons: Help/Find Action/Registry/Select/Type: merge buttons/Unselect
- Avoid push confirmation: In push confirmation dialog uncheck checkbox
- Terminal load .env: File/Settings/Tools/Terminal/Shell path: /bin/bash --init-file .env
- Wide screen: Appearance and Behavior/Appearance
- JDK: /opt/openjdk@xxx/libexec/openjdk.jdk/Contents/Home"
- Weird symbols non english lang: Settings \ Advanced Settings \ Editor \ Checkbox for
  "Render special characters (control codes, etc) using their Unicode name abbreviations"
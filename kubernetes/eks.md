# Elastic Kubernet Service

## Cluster not deleted

Go to cloudformation, select cluster and deleted. If fail check the resources used by it and delete manually

## Autoscaling 

eksctl create cluster --name=cluster-5 --nodes-min=3 --nodes-max=5 # Need install and configure Auto Scaling

## Associate IAM role to serviceaccount on any namespace

1. Create policy to give permissions to the IAM role
1. Create iam role with police and associate to serviceaccount on some namespace:

    ```shell
    eksctl create iamserviceaccount \
        --name eks-access-yieldbot-secrect1 \
        --namespace default \
        --cluster $NAME \
        --attach-policy-arn "arn:aws:iam::###:policy/xxx" \
        --approve \
        --override-existing-serviceaccounts \
        --region eu-central-1
    ```

1. Find the associate IAM role, remove the condition that fixed the namespace in the trust relationship 
   and take it arn

1. Associate the IAM role to a service account on other namespace

    ```shell
    kubectl annotate serviceaccount -n $NAMESPACE   $SERVICEACCOUNT $ROLEARN      
    ```
   
## Dashboard

https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html

Get dashboard token

## Commands

* STACK_NAME=$(eksctl get nodegroup --cluster $CLUSTER_NAME -o json --region eu-central-1 | jq -r '.[].StackName')
* ROLE_NAME=$(aws cloudformation describe-stack-resources --stack-name $STACK_NAME --region eu-central-1 |
  jq -r '.StackResources[] | select(.ResourceType=="AWS::IAM::Role") | .PhysicalResourceId')
* aws iam attach-role-policy --role-name $ROLE_NAME
  --policy-arn arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy
* Check policies: aws iam list-attached-role-policies --role-name $ROLE_NAME |
  grep CloudWatchAgentServerPolicy || echo 'Policy not found'

## Update cluster
  
  
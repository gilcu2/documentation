# Kubernetes

## Commands

* Execute a console command in a running pod: kubectl exec -it \<pod-name> -- bash
* Execute a console command in a new pod: kubectl run bash --image=\<image> -i --tty
* Resume shell session: kubectl attach <pod-name> -c bash -i -t
* Logs of container: kubectl logs \[--previous] \[--follow] \[--tail=1200] <pod-name>
* List services: kubectl get services
* List services all namespaces:   kubectl get svc --all-namespaces
* Delete pod: kubectl delete pod <pod-name>
* Debug pod: kubectl get pod <podname> \[-o yaml]
* List of deleted pod:  kubectl get event -o custom-columns=NAME:.metadata.name | cut -d "." -f1
* Show pods and nodes: kubectl get pod -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --all-namespaces
* Show nodes, namespaces and pods: 
  kubectl get pod -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.namespace,NAME:.metadata.name --all-namespaces
* Get all objects in namespace:  kubectl get all -n $NAMESPACE
* Set current namespace: kubectl config set-context --current --namespace=$NAMESPACE
* Show current namespace: kubectl config view --minify --output 'jsonpath={..namespace}'; echo
* Get contexts: kubectl config get-contexts 
* Rename context: kubectl config rename-context old-name new-name
* Available resources: kubectl describe nodes
* Copy files: kubectl cp \[<namespace>/]\[<pod>]<file-spec-src> <file-spec-dest>
* Show pod image:  kubectl get pods -o jsonpath='{range .items[*]}{"\n"}{.metadata.name}{":\t"}{range
  .spec.containers[*]}{.image}{", "}{end}{end}'
* Get secret:  echo -n $(kubectl get secret mongodb-connections-service-user -o json | jq '.data."mongodb-connections.json"') | sed 's/"//g' | base64 -d


## Interesting cluster packages

* kedacore/keda: Allows scalate pods based in multiple measures
* stakater/reloader: Reload pod on configuration changes
* metrics-server: Collect metrics
* dashboard: Show metrics
* prometheus-community/kube-prometheus-stack: Monitoring and alerts

## Dashboard

* Move to kubernetes dashboard namespace
* Get token: kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | awk '/^deployment-controller-token-/{print $1}') | awk '$1=="token:"{print $2}'

* kubectl proxy
* http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

## Debug container

Set command:
  ```yaml
  command: [sleep]
  args: ["30000"]
  ```

and run shell into it

## Redeploy job needs delete previous
  
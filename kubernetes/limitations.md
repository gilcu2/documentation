# Kubernetes limitations

* Not able to take environment values in yaml. Docker-compose do. Can be done with configmap and secrets
* Not able to easy expose a service (Nodeport) in any port

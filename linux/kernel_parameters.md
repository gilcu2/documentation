# Kernel parameters

- video=3840x2400-32@60
- acpi_osi='Windows 2015'  # 2015, 2022 
- acpi.ec_no_wakeup=1
- nvme_load=YES
- nvidia-drm.fbdev=1
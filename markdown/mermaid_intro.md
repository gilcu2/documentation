# Mermaid: Diagrams as code

* Renders Markdown-inspired text definitions to create and modify diagrams dynamically
* Documentation catch up with Development

## Example

```mermaid
graph LR
  A[Seed curation] -->  B[(Modelin)] --> C([Consolidation])
  
```

## Diagram types

* Graph
* Flowchart
* Sequence
* Class
* State
* Entity Relation
* User Journey
* Pie
* ...

## Graph

* Graph: graph \<Orientation:TB|TD|BT|RL|LR>
* Node: \<Identifier>\<Shape begin>\<Label>\<Shape end>
* Shapes:

  * \[] Box
  * () Roundex box
  * \[()] Database
  * (()) Circle
  * {} Rhombus
  * {{}} Hexagon
  * \[//] Paralelogram
  * ...

* Links:

  * -->
  * ---
  * --Text--
  * -->|Text|
  * -.Text.->
  * ...

```mermaid
graph RL
  A[Box] -->  B(Roundex Box) --- C[(DB)] ---|Hi| D((Circle))
  E{Rhombus} -->|Hola| F{{Hexagon}} -. Привет всем! .- G[/Paralelogram/] --> F
  
```

* Chaining links

```mermaid
graph TB
  a --> b & c--> d
  A & B--> C & D
```

* Subgraphs

```mermaid
graph TB
  a --> b & c--> d
  A & B--> C & D
```

* Other features in graphs:

  * Arrow types
  * Multi directional arrows
  * Subgraphs
  * ...

* Use in IDEA

  * Markdown enhanced  navegator

* Use in confluence

   * Need [plugin](https://marketplace.atlassian.com/apps/1222792/mermaid-integration-for-confluence?hosting=cloud&tab=overview)
  * [Example](https://pubnativegmbh.atlassian.net/wiki/spaces/~174568047/pages/1804009567/Mermaid+in+confluence)

* Conclusions

  * Recommend Mermaid for code documentation

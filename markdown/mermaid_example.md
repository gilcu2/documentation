# Mermaid rendering

Check mermaid rendering

```mermaid
graph TD
  A[curate_age_seeds] -->  B[curate_gender_seeds] -->C[write_audience_report]
  C --> E[create_app_dictionary_freqs]
  E --> F[create_app_dictionary] --> G[model_rtb_gender]
  G --> H[model_combined_gender] --> I[finalize_gender_audiences]
  I --> J[write_monthly_change_reports]
  
```
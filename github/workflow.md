# Workflow

## Docker buildkit cache 

    * Follow https://gist.github.com/UrsaDK/f90c9632997a70cfe2a6df2797731ac8
    * With:
    
``` yml
jobs:
  test:
    runs-on: ubuntu-latest
     env:
       BUILD_CACHE: /home/runner/.docker/buildkit

     steps:
       - name: Checkout code
         uses: actions/checkout@v2
       - name: Compute cache key
         run: echo "CACHE_KEY=$(git ls-files -s dir1 dir2 | git hash-object --stdin)" >> $GITHUB_ENV
       - name: Cache docker layers
         uses: actions/cache@v1
         with:
           path: ${{ env.BUILD_CACHE }}
           key: cache-${{ env.CACHE_KEY }}
           restore-keys: |
             cache-
```

## Worflow control

- Run manualy/disable:
  ```
  on: [workflow_dispatch]
  ```
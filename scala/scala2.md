# Scala 2

- Matching List of different types:
  ```scala
    def checkType[T: TypeTag](v: T) = typeOf[T] match {
      case t if t =:= typeOf[List[String]] => "List of Strings"
      case t if t =:= typeOf[List[Int]] => "List of Ints"
    }
  ```

- for with yield is a sequence of flatMaps ending with a map.
  It generate the type of the first generator with values of the yield
  ```scala
  for {
    i1 <- g1
    i2 <- g2
    i3 <- g3
  } yield (i1,i2,i3)
  ```
  generate all combinations in a contained of type g1

It is equivalent to

```scala
g1
 .flatMap(i1 => 
    g2.flatMap(i2 => 
        g3.map(i3 =>
          (i1,i2,i3)
        )
    )
 )         
```

- for definition
  The precise meaning of generators and guards is defined by translation to invocations of four methods:
  map, filter, flatMap, and foreach. These methods can be implemented in different ways for different carrier types.

The translation scheme is as follows. In a first step, every generator p <- e, where p is not irrefutable (§8.1)
for the type of e is replaced by

p <- e.filter { case p => true; case _ => false }

Then, the following rules are applied repeatedly until all comprehensions have been eliminated.

    A for-comprehension for (p <- e) yield e0 is translated to e.map { case p => e0 }.

    A for-comprehension for (p <- e) e0 is translated to e.foreach { case p => e0 }.

    A for-comprehension for (p <- e; p0 <- e0 . . .) yield e00, where . . . is a (possibly empty) sequence of generators or guards, is translated to:
    e.flatMap { case p => for (p0 <- e0 . . .) yield e00 }.

    A for-comprehension for (p <- e; p0 <- e0 . . .) e00 where . . . is a (possibly empty) sequence of generators or guards, is translated to:
    e.foreach { case p => for (p0 <- e0 . . .) e00 } .

    A generator p <- e followed by a guard if g is translated to a single generator:
    p <- e.filter((x1, . . . , xn) => g )
    where x1, . . . , xn are the free variables of p.

    A generator p <- e followed by a value definition val p0 = e0 is translated to the following generator of pairs of values, where x and x0 are fresh names:

    val (p, p0) <- 
      for(x@p <- e) yield { val x0@p0 = e0; (x, x0

- Another for explanation
  for comprehension can contain the following three expressions
- Generators
- Filters
- Definitions
  , but begins with a generator that determine the result type (elements of results are given by yield)

```scala
for {
    p <- persons             // generator
    n = p.name               // definition
    if (n startsWith "To")   // filter
} yield
```



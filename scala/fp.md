# Functional programming

- local reasoning
- composition

Algebraic Data Types (ADT) are composition of AND/OR. AND define tle properties of an
data type and OR the types of data types

```scala3
enum Tree[A] {
    case Leaf(value: A)
    case Node(left: Tree[A], right: Tree[A])
}
```
# Unity3d rendering

- Make object invisible: gameObject.GetComponent(MeshRenderer).enabled = false;
- Disable object:  gameObject.active = false;
- Raycast: Rect. From a camera:  camera.ViewportPointToRay(new Vector3 (0.5f, 0.5f, 0));
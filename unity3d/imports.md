# Imports

- Material from fbx:

  Select it in the project pane, open the Materials tab in the inspector 'Import Settings' pane, select 'Use External
  Materials (Legacy)' and click apply. Once done, you can set the location option back to 'Use Embedded Materials'.
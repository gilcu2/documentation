# Screen

Allows multiple terminals in one that keep alive between login sessions

## Problem -> solutions

*  Cannot open your terminal '/dev/pts/0' - please check"

    Run script /dev/null to own the shell (more info over at Server Fault); then try screen again. [Link](https://makandracards.com/makandra/2533-solve-screen-error-cannot-open-your-terminal-dev-pts-0-please-check)

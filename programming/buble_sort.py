"""Write a small python function that implements BubbleSort and sorts a list with integers in an ascending order.
Bubble Sort works by repeatedly swapping the adjacent elements if they are in wrong order.
What is the time and memory complexity?
"""


def bubble_sort(arr):
    for index in range(len(arr)):
        for index1 in range(len(arr) - 1):
            if arr[index1] > arr[index1 + 1]:
                tmp = arr[index1]
                arr[index1] = arr[index1 + 1]
                arr[index1 + 1] = tmp

    return arr


def main():
    # define list
    unsorted_list = [64, 34, 25, 12, 22, 11, 90]
    sorted_list = [11, 12, 22, 25, 34, 64, 90]

    # call bubble_sort
    result = bubble_sort(unsorted_list)
    print(result)
    assert sorted_list == result


if __name__ == '__main__':
    main()

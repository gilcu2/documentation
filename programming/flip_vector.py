"""Write a method that takes a 3d vector, creates a unit vector from it and flips its direction. 
Afterwards, return the flipped vector."""
import math


def flip_vector(vector):
    # calculate unit vector
    norm = math.sqrt(vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2)
    unitary = [vector[0] / norm, vector[1] / norm, vector[2] / norm]
    # flip the vector
    flip = [-unitary[0], -unitary[1], -unitary[2]]
    return flip


def main():
    # define your vector here
    # for example x,y,z = 3,3,3
    init_vector = [1, 2, 3]  # TODO
    flipped_vector = flip_vector(init_vector)
    print(flipped_vector)


if __name__ == '__main__':
    main()

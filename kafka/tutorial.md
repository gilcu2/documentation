# Apacha Kafka 3 Tutorial

## Features

- Events (records) have unique id, time and data
- Events belong ta a topic
- Producer and consumers are decoupled
- Events can be read several time by different consumers, only deleted by live time
- Topics are partitioned by key to buckets located in different brokers (servers) for achieve scalability
- Events are consumed in production order
- Topics are replicated for fault-tolerant and highly-availability at partition level (3 replicas default)
- Consumer, producer and transformation API
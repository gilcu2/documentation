Keep the changes of the project. Classic CHANGELOG.md file

## Types of changes 
 
- Added: for new features.
- Changed: for changes in existing functionality.
- Deprecated: for soon-to-be removed features.
- Removed: for now removed features.
- Fixed: for any bug fixes.
- Security in case of vulnerabilities.

## Sample

https://raw.githubusercontent.com/jerry-git/pytest-split/master/CHANGELOG.md
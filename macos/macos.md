# Mac OS

- Show hidden files: CMD+SHIFT+.
- Take screenshots: press Shift-Command-5
- Setup initial login screen keyboard
  1.  In your user account, make the keyboard layout have only the one you want at login. 
    To do this, go to System Settings>Keyboard>Input Sources
  1. sudo cp ~/Library/Preferences/com.apple.HIToolbox.plist /Library/Preferences/
  2. Reboot

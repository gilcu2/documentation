# Pyspark

## Use externall hadoop with pyspark

```shell
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk/jre/"
export SPARK_HOME="$HOME/opt/spark-3.0.2-bin-without-hadoop"
#export SPARK_HOME="$HOME/opt/spark-3.0.2-bin-hadoop3.2/"
export HADOOP_HOME="$HOME/opt/hadoop-3.2.2"

export SPARK_DIST_CLASSPATH="$HADOOP_HOME/etc/hadoop/*:\
$HADOOP_HOME/share/hadoop/common/lib/*:\
$HADOOP_HOME/share/hadoop/common/*:\
$HADOOP_HOME/share/hadoop/hdfs/*:\
$HADOOP_HOME/share/hadoop/hdfs/lib/*:\
$HADOOP_HOME/share/hadoop/hdfs/*:\
$HADOOP_HOME/share/hadoop/yarn/lib/*:\
$HADOOP_HOME/share/hadoop/yarn/*:\
$HADOOP_HOME/share/hadoop/mapreduce/lib/*:\
$HADOOP_HOME/share/hadoop/mapreduce/*:\
$HADOOP_HOME/share/hadoop/tools/lib/*"

PYTHONPATH=".:./emr/src:$PYTHONPATH"
PYTHONPATH=$(ZIPS=("$SPARK_HOME"/python/lib/*.zip); IFS=:; echo "${ZIPS[*]}"):$PYTHONPATH
export PYTHONPATH

export PATH="$JAVA_HOME/bin/:$SPARK_HOME/bin:$HADOOP_HOME/bin:$PATH"

#export PATH="/usr/lib/jvm/java-11-openjdk/jre/bin/:$PATH"
#export PYSPARK_HADOOP_VERSION=3.2

export PYSPARK_SUBMIT_ARGS="--master local[*] \
--jars https://s3.amazonaws.com/redshift-downloads/drivers/jdbc/1.2.36.1060/RedshiftJDBC42-no-awssdk-1.2.36.1060.jar \
--packages \
org.apache.hadoop:hadoop-aws:3.2.1,\
mysql:mysql-connector-java:8.0.25,\
io.github.spark-redshift-community:spark-redshift_2.12:5.0.3 \
pyspark-shell"

#export ARN_SECRETS_PREFIX=arn:aws:secretsmanager:eu-central-1:981939790834:secret:
#export AWS_PROFILE=tracking
```



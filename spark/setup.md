# Spark setup

## Install with specific hadoop version 

Following  https://serge-m.github.io/posts/spark-on-a-local-machine/ 

1. export JAVA_HOME
1. Download hadoop and unpack, export HADOOP_HOME=...
1. Download spark without hadoop and unpack, export SPARK_HOME=
1. export SPARK_DIST_CLASSPATH="$HADOOP_HOME/etc/hadoop/*:\\
$HADOOP_HOME/share/hadoop/common/lib/*:\\
$HADOOP_HOME/share/hadoop/common/*:\\
$HADOOP_HOME/share/hadoop/hdfs/*:\\
$HADOOP_HOME/share/hadoop/hdfs/lib/*:\\
$HADOOP_HOME/share/hadoop/hdfs/*:\\
$HADOOP_HOME/share/hadoop/yarn/lib/*:\\
$HADOOP_HOME/share/hadoop/yarn/*:\\
$HADOOP_HOME/share/hadoop/mapreduce/lib/*:\\
$HADOOP_HOME/share/hadoop/mapreduce/*:\\
$HADOOP_HOME/share/hadoop/tools/lib/*"
1. export   PYTHONPATH=$(ZIPS=("$SPARK_HOME"/python/lib/*.zip); IFS=:; echo "${ZIPS[*]}"):$PYTHONPATH
1. export PATH="$JAVA_HOME/bin/:$SPARK_HOME/bin:$HADOOP_HOME/bin:$PATH"

## Install standalone cluster in a single node

1. Enable ssh login without passwd in localhost
1. Setup environment in $SPARK_HOME/conf/spark-env.sh like 
   * JAVA_HOME
   * SPARK_WORKER_CORES
   * SPARK_WORKER_MEMORY
   * PYSPARK_PYTHON
   * PYSPARK_PYTHON
1. Set $SPARK_HOME/conf/slaves to repetitions of localhost for each worker you want
1. Run 
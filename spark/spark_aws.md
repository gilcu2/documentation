# Spark AWS 

## Connect to redshift with a different role from spark (must work for any service and client)
    
```python
    import boto3
    import os
    from pyspark.sql import SparkSession
    
    sts_client = boto3.session.Session().client('sts')
    credentials = sts_client.assume_role(RoleArn='arn:aws:iam:<region>:<role id>:<resource type>/<resource>',
                                         RoleSessionName='session_name')['Credentials']

    os.environ['AWS_ACCESS_KEY']=credentials['AccessKeyId']
    os.environ['AWS_SECRET_KEY'] = credentials['SecretAccessKey']
    os.environ['AWS_SESSION_TOKEN'] = credentials['SessionToken']

     spark = SparkSession.builder ...
```

    Another option is:

```python
    spark.sparkContext._jsc.hadoopConfiguration().set('fs.s3a.aws.credentials.provider',
                                                       'org.apache.hadoop.fs.s3a.TemporaryAWSCredentialsProvider')
    spark.sparkContext._jsc.hadoopConfiguration().set('fs.s3a.access.key', credentials['AccessKeyId'])
    spark.sparkContext._jsc.hadoopConfiguration().set('fs.s3a.secret.key', credentials['SecretAccessKey'])
    spark.sparkContext._jsc.hadoopConfiguration().set('fs.s3a.session.token', credentials['SessionToken'])
```
    but doesn't work on some library version and clients , for example in io.github.spark_redshift_community.spark.redshift 1. 
- all indices:  GET _cat/indices
- index defintion:
GET master.devices.initial-sync-registry-indexing
{
  "query": {
    "match_all": {}
  }
}


- get all objects:
GET stable.n4.search.metacompanysearch.contactindex-indexing/_search
{
  "query": {
    "match_all": {}
  }
}
- Filter by update on
GET stable.n4.search.metacompanysearch.goodsoutsindex-indexing/_search
{
  "query": {
     "bool": { 
      "filter": [
        { "range": { "updatedOn": { "gte": "2023-07-18T08:05" }}}
      ]
    }
  },
  "stored_fields" : ["_metaDO","_metaRef","lastUpdated"]
}

- Exact field value
GET stable.n4.search.metacompanysearch.goodsoutsindex-indexing/_search
{
  "query": {
    "match": {
      "_metaRef": "n4://organization:41203b33779bb5763f65423a/deliveries_pickup/669a707b7eb79f7ae772914b"
    }
  },
"stored_fields" : ["_metaDO","_metaRef","lastUpdated"]
}


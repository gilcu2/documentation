# SQL

- Change NULL to non null value: COALESCE
  ```SQL
  select c.NAME , COALESCE(a.count_employee,0)  from company c left join
  (select e.company_id as company_id, count(*) as count_employee from employee e group by e.company_id) a
  ON  a.company_id =  c.id;
  ```

- Take full row from a group
  ```SQL
  select s0.*
  from share as s0
  inner join (
     	select publisher_id, max(datum) as datum
       	from share
           	group by publisher_id
  ) as latest_per_publisher
  on s0.publisher_id = latest_per_publisher.publisher_id 
  ```
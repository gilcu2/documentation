# Install Zepp tooling

Allows creating apps and watch faces for Zepp OS devices like GTR 3 Pro. Require Ubuntu 20.04 machine

# Simulator

```shell
wget https://upload-cdn.huami.com/zeppos/simulator/download/setup.sh
wget https://upload-cdn.huami.com/zeppos/simulator/download/simulator_1.0.6_amd64.deb # Check latest version
sudo sh setup.sh  # Install dependencies
sudo apt install -f simulator_1.0.6_amd64.deb
sudo chmod -R 777 /opt/simulator
```

Run:

```shell
/opt/simulator/simulator # Terminal shows: DevTools listening on ws://127.0.0.1:XXXX...
# Don't close terminal
# Home key unblock virtual watch
```

## Cli

Default version in Ubuntu 20.04 has old version of nodejs that fails when installed the cli. First need to be installed
a modern version as shown here.

Install:

```shell
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm i @zeppos/zeus-cli -g  # some warnings
```

Test:

```shell
/opt/simulator/simulator 
zeus create hello-world # In a new terminal
cd hello-world
zeuz login
zeus dev # App can be found inn the list and run
```

After some time you
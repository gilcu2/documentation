# Creating a Programming Language: Intro

Creating a programming language can be divided in 6 parts:

1. Parsing
2. Performing Symbol Resolution
3. Creating a Type system
4. Validation
5. Optimization
6. Interpreting / Compiling the code

We are going to list and define these 6 parts in this lesson. During the rest of the course we will explain them in more
detail.

Parsing means transforming the text, as it is written by the developer, in code that can be understood and manipulated
by the compiler. This transformation only involves syntax. So, at the end of this phase you get a representation of the
structure of the code. E.g., you know that something like int num defines a declaration, but you do not yet know that
all the mentions of num in the code represent the same thing.

Performing Symbol Resolution entails taking all the references of variables, functions, fields, etc. and link them
together appropriately. Eventually, you have also to find any potential issue, like the use of a variable that is not
declared. For instance, going to back at our example of num, at the end of this process you get that all references to
num are connected to the same entity, a variable.

Creating a Type System requires to create a series of rules to assign a type to any expression of the language or a
variable. The rule determines whether the operation is valid and, if so, its type. For example, can you sum an integer
and a float? If it is possible, which is the type of the result?

Validation is a step that starts at the same time of the two previous steps. When you perform symbol resolution and
assign a type to an expression you also start validation. For example, if an operation results in an invalid type you
get a validation error; if a declaration is missing you get a validation error in the symbol resolution phase. However,
validation involves more: it also means performing more complex semantic validations. For example, many languages
require to have a main function somewhere in the code, otherwise it is not valid program.

Optimization is a step that involves manipulation of the code to create something that does the same thing, but more
efficiently. For example, some form of recursion can be transformed in a more efficient loop.

Interpreting or Compiling the code is the step in which you execute the code or prepare it for a later execution. This
is literally where things happen, so it is arguably the most important step.

Finally, we will also talk about creating editors for your language. While this is not strictly necessary to have a
language, it is fundamental nowadays to be productive.

That is true even if you just want to create a programming language for your own benefit. Think about it, how many
developers like to code without syntax highlighting, autocomplete, and all the modern amenities of code editors?

Now that you know the big picture, you are ready to start working on creating the programming language. We are going to
do it, in the next lesson that you will receive in 3 days.

The path is set for our journey, at the end lies the prize: creating a programming language. Along the way, a few
interesting lessons.




# Creating a Programming Language: Parsing

Parsing means transforming the text, as it is written by the developer, in code that can be understood and manipulated
by the compiler.

That’s what we said in the previous lesson. In practice this means that the text that the developer writes get
transformed, according to syntactical rules, in a parse tree.

What parsing does
A syntactical rule transforms a bunch of text into a logical element, e.g., a series of letters become an identifier,
i.e., the name, of a variable or a function.

The parse tree is a hierarchical representation of code, for example a function contains a list of statements, the first
statements is a declaration of a variable, the declaration contains an initialization, and so on.

From text to parse tree

Parsing is divided in two phases: lexing and the proper parsing.

A lexer and a parser work in sequence: the lexer scans the input and produces the matching tokens, the parser then scans
the tokens and produces the parsing tree.

Imagine the text: 437 + 734.

The lexer scans the text and find 4, 3, 7 and then a space ( ). The job of the lexer is to recognize that the characters
437 constitute one token of type NUM. Then the lexer finds a + symbol, which corresponds to a second token of type PLUS,
and lastly it finds another token of type NUM.

These series of tokens are then sent to the parser. The parser assembles them to form the nodes of the tree. In this
example the parser would recognize that the series of tokens NUM PLUS NUM corresponds to a sum.

You could build the lexer and the parser by hand, but luckily you do not have to. There are many tools out there that
can help generating both a lexer and a parser from a description of the language. In our opinion the best one is ANTLR.
We use it to create a lexer and a parser for a language in two articles:

Getting started with ANTLR: building a simple expression language

Building and testing a parser with ANTLR and Kotlin

If you want to get a complete tutorial on ANTLR (15K+ words!) you should read:

ANTLR Mega Tutorial

In these two articles you will find the code to create a lexer and a parser. We are going also to see the equally
important task of testing them. Testing is important in all programs, but it is absolutely essential when building a
language. That’s because if there is a bug in the parser of a language all programs could be affected. Think about that.

Parsing is the fundamental step in creating a programming language. It is also the one skill that you can use for a lot
of other things, such as parsing data from files or any other source. Personally, I find parsing fascinating, almost
magical: before you just have a piece of text, but with parsing you translate this simple text in something
understandable and manageable by computer.

In the next lesson we are going to see how to use our lexer and parser to manipulate the parse tree in order to perform
symbol resolution, validation and optimization.
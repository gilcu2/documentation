# Camera Linux

Managed with v4l2

## Camera properties

```shell
v4l2-ctl -d /dev/video5 --list-formats-ext
```

## Virtual camera with rotate video by loopback: [Source](https://superuser.com/questions/594602/rotate-webcam-video-on-ubuntu-linux)

1. Startup load module with parameters:
    ```shell
    #  /etc/modules-load.d/v4l2loopback.conf
    v4l2loopback
    
    # /etc/modprobe.d/v4l2loopback.conf
    options v4l2loopback devices=1 exclusive_caps=1
    ```

1. Rotate with ffmped

## Virtual camera with rotate video by hardware:

```shell
v4l2-ctl -d 4 -v width=720,height=1280,pixelformat=H264 --set-ctrl=rotate=90
```


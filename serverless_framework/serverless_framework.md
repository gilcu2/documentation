# Serverless framework

Deploy cloud serverless apps

- Install/update

npm install -g serverless

- Deploy on iam role

In some version using --aws-profile or equivalents fail.
One solution is create temporary credential with aws sts and define env vars to using it.

```shell
aws sts assume-role --role-arn <role>  --role-session-name <name> # This print the credentials
 export AWS_ACCESS_KEY_ID=...
 export AWS_SECRET_ACCESS_KEY=...
 export AWS_SESSION_TOKEN=...
 sls deploy ...
```


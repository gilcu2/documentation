# pip

- Uninstall all packages

pip freeze | xargs pip uninstall -y

- Install for specific python version

python3.10 -m ensurepip

- Update

sudo -H pip3 install --upgrade pip
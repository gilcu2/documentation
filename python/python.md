# Python

- Check main:
  ```python
  if __name__ == '__main__':
    pass
  ```
- f interpolation format:
  - Cero refill:  f'{n:03}'
- Thread that return value in join
  ```python
  from threading import  Thread
  from option import Ok, Err, Result
  from datetime import datetime
  
  class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, Verbose=None):
        if kwargs is None:
            kwargs = {}
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            print(f'Running {self._target} with {self._args} {datetime.now()}')
            try:
                self._return = self._target(*self._args,
                                            **self._kwargs)
            except Exception as e:
                self._return = Err(f'Exception {e}')
            print(f'Finish Running {self._target} with {self._args} {datetime.now()}')

    def join(self, *args) -> Result[str, str]:
        begin = datetime.now()
        print(f'Waiting {self} {begin}')
        Thread.join(self, *args)
        end = datetime.now()
        print(f'{self} finish {end} {end - begin}')
        return self._return

   ```


- Take values from environment file

 ```python
from dotenv import load_dotenv

load_dotenv()
 ```
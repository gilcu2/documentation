# pycharm

- pytest: Settings/Preferences | Tools | Python Integrated Tools | Default test runner | pytest

- Environment vars for running in IDE.Solution tested with virtual environment.

 ```python
# pip install python-dotenv
from dotenv import load_dotenv

load_dotenv()
 ```

- Environment vars for running in console.Solution tested with virtual environment.
  sourve <env file> in venv/bin/activate
- Debugging async or coverage test
  Edit configuration and pass --no-cov in Additional Arguments

- Compatibility of code inspection with python versions
  Settings/Editor/Inspection/Code is compatible with specific python version
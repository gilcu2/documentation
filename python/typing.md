# Python typing

# Callable

Can be called like a function

- Fixed Specific types: Callable[[Arg1Type, Arg2Type], ReturnType]
- Accepting any arguments: Callable[..., ReturnType]